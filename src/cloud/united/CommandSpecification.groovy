package cloud.united

/**
 * @author vladimir.dabic
 * Object used for creating specifications used for execution of Gradle commands.
 */
public class CommandSpecification implements Serializable  {

    /**
    * Full gradle command to be executed e.g. ./gradlew clean build
    */
    String command;
    /**
    * List of strings representing profiles needed for command execution. e.g. test,it_test...
    */
    List profiles;
    /**
    * Log level for gradle command e.g. --info
    */
    String logLevel;
    /**
    * List of strings representing parameters, with their keys and values separated by = sign
    * needed for command execution e.g. -Pclassifier=RELEASE, -PbuildNumber=0...
    */
    List parameters;
    /**
    * List of strings representing gradle tasks e.g. sonarqube, jacocoTestReports, dependencyCheck...
    */
    List jobs;
    /**
    * Default parameters extracted from the project class
    * -Pclassifier, -PbuildNumber, -Dsonar.branch.name, -Pnexus.username, -Pnexus.password
    */
    def defaultParameters = [:];
    /**
    * Default list of gradle tasks
    */
    List defaultJobs;
    Project project;

    CommandSpecification(Project project) {

        this.project = project;
    }

    CommandSpecification withProfiles(List profiles) {

        this.profiles = profiles;
        return this;
    }

    CommandSpecification withLogLevel(String logLevel) {

        this.logLevel = logLevel;
        return this;
    }

    CommandSpecification withCommand(String command) {

        this.command = command;
        return this;
    }

    /**
    * Accepts list of strings, one string represents key value pair separated by equals sign. e.g. -Pclassifier=RELEASE
    * If specified all default paramteres with the given keys will be removed.
    */
    CommandSpecification withParameters(List parameters) {

        this.parameters = parameters;
        for (String parameter : parameters) {
            String[] keyValuePair = parameter.split("=");
            if (defaultParameters[keyValuePair[0]] != null) {
                defaultParameters.remove(keyValuePair[0]);
            }
        }
        return this;
    }

    CommandSpecification withDefaultParameters() {

        defaultParameters['-Pclassifier'] = project.getClassifier();
        defaultParameters['-PbuildNumber'] = project.getBuildNumber();
        defaultParameters['-Dsonar.branch.name'] = project.getBranch();
        defaultParameters['-Pnexus.username'] = project.getNexusUsername();
        defaultParameters['-Pnexus.password'] = project.getNexusPassword();
        return this;
    }

    CommandSpecification withMergeRequestDefaultParameters() {

        defaultParameters['-PbuildNumber'] = "sonar-${project.getBuildNumber()}";
        defaultParameters['-Dsonar.branch.name'] = project.getBranch();
        defaultParameters['-Pnexus.username'] = project.getNexusUsername();
        defaultParameters['-Pnexus.password'] = project.getNexusPassword();
        return this;
    }

    CommandSpecification withJobs(List jobs) {

        this.jobs = jobs;
        return this;
    }

    CommandSpecification withDefaultJobs() {

        defaultJobs = ["jacocoTestReport", "dependencyCheckAnalyze", "sonarqube"];
        return this;
    }

    String toCommandString() {
        
        String result = "";
        result += profilesToString() ? profilesToString() + " " : "";
        result += command ? command + " " : "";
        result += logLevel ? logLevel + " " : "";
        result += defaultJobsToString() ? defaultJobsToString() + " " : ""; 
        result += jobsToString() ? jobsToString() + " " : "";
        result += defaultParametersToString() ? defaultParametersToString() + " " : "";
        result += parametersToString();
        return result;
    }

    String profilesToString() {

        String result = "";
        if (profiles == null || profiles.size() == 0) {
            return result;
        }
        result += "SPRING_PROFILES_ACTIVE=";
        for (String profile : profiles) {
            result += profile + ",";
        }
        return result.substring(0, result.size() - 1);
    }

    String defaultJobsToString() {

        String result = "";
        if (defaultJobs == null || defaultJobs.size() == 0) {
            return result;
        }
        for (String job : defaultJobs) {
            result += job + " ";
        }
        return result.substring(0, result.size() - 1);
    }

    String jobsToString() {

        String result = "";
        if (jobs == null || jobs.size() == 0) {
            return result;
        }
        for (String job : jobs) {
            result += job + " ";
        }
        return result.substring(0, result.size() - 1);
    }

    String parametersToString() {

        String result = "";
        if (parameters == null || parameters.size() == 0) {
            return result;
        }
        for (String parameter : parameters) {
            result += parameter + " ";
        }
        return result.substring(0, result.size() - 1);
    }

    String defaultParametersToString() {

        String result = "";
        if (defaultParameters.size() == 0) {
            return result;
        }
        defaultParameters.each{entry -> result += "${entry.key}=${entry.value} "};
        return result.substring(0, result.size() - 1);
    }
}