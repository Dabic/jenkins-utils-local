package cloud.united

import cloud.united.CommandSpecification

/**
 * @author vladimir.dabic
 */
public class Project implements Serializable {

    def jenkinsContext;
    ProjectType projectType;
    String artifactVersion;
    String buildNumber;
    String classifier;
    String branch;
    String nexusUsername = "asset-downloader";
    String nexusPassword = "password";
    String dockerRegistry = "https://docker.united.cloud"

    Project(jenkinsContext, ProjectType projectType, String branch, String buildNumber) {
        this.jenkinsContext = jenkinsContext;
        this.projectType = projectType;
        this.branch = branch;
        this.buildNumber = buildNumber;
    }

    void checkOutFrom(String branch, String url, String credentialsId) {
        jenkinsContext.git branch: branch, url: url, credentialsId: credentialsId
        artifactVersion = extractArtifactVersion();
        classifier = branch == 'master' ? 'RELEASE' : 'SNAPSHOT';
    }

    void testBuild(CommandSpecification specification) {
        jenkinsContext.sh specification.toCommandString();
    }

    void publishBuildToNexus(CommandSpecification specification) {
        jenkinsContext.sh specification.toCommandString();
    }

    void buildAndPushDockerImage() {
        def dockerImage = buildDockerImage();
        dockerImage.push();
    }

    void pushGitTag() {
        jenkinsContext.sshagent(['jenkins-uc-global-gitlab-write-access']) {
            sh """
            git tag ${getVersion()}
            git push origin --tags
            """
        }
    }

    def buildDockerImage() {
        def exists = fileExists dockerFile
        if (exists) {
            docker.withRegistry("${dockerRegistry}", 'docker-public') {
                return docker.build("${projectType.getDockerImageName()}:${getVersion()}", " -f ${projectType.getDockerFile()} .")
            }
        } else {
            error "Dockerfile not found"
        }
    }

    String extractArtifactVersion() {
        return jenkinsContext.sh(script: 'cat gradle.properties | awk -F"ARTIFACT_VERSION=" \'/ARTIFACT_VERSION=/{print $2}\'', returnStdout: true).toString().trim()
    }

    String getVersion() {
        return "${artifactVersion}-${buildNumber}-${classifier}";
    }
}