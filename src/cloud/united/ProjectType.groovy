package cloud.united

public enum ProjectType {
    VMS('VMS', 'https://gitlab.com/Dabic/demo.git', 'vms', 'Dockerfile'),
    CACHING_PROXY('CACHING-PROXY', 'git@gitlab.united.cloud:uc-infoservice/uc-caching-proxy.git', 'uc-caching-proxy', 'Dockerfile');

    final String name;
    final String repository;
    final String dockerImageName;
    final String dockerFile;

    private ProjectType(String name, String repository, String dockerImageName, String dockerFile) {
        this.name = name;
        this.repository = repository;
        this.dockerImageName = dockerImageName;
        this.dockerFile = dockerFile;
    }

    String getRepository() {
        return repository;
    }

    String getDockerImageName() {
        return dockerImageName;
    }

    String getDockerFile() {
        return dockerFile;
    }
}