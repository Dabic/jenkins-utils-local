package cloud.united

import java.text.SimpleDateFormat
/**
 * @author vladimir.dabic
 */
class Notifications {

    public class Logger {

        Project project;

        public Logger(Project project) {

            this.project = project;
        }

        void info(String message) {

            project.getJenkinsContext().echo(decorateMessage(message, "INFO"))
        }

        void warn(String message) {

            project.getJenkinsContext().echo(decorateMessage(message, "WARN"))
        }

        void error(String message) {

            project.getJenkinsContext().echo(decorateMessage(message, "ERROR"))
        }

        String decorateMessage(String message, String logLevel) {

            return "[${sdf.format(date)} ${logLevel}] ${message}";
        }

    }

    public class SlackNotifications {

        Project project;

        public SlackNotifications(Project project) {

            this.project = project;
        }

        void info(String channel, String message) {

            project.getJenkinsContext().slackSend channel: channel, color: 'good', message: decorateMessage(message), tokenCredentialId: 'SlackToken'
        }

        void error(String channel, String message) {

            project.getJenkinsContext().slackSend channel: channel, color: 'danger', message: decorateMessage(message), tokenCredentialId: 'SlackToken'
        }

        String decorateMessage(String message) {

            return "*[${project.getProjectType().getName()}]* ${message}"
        }
    }

    public class JiraNotification {

        Project project;
        Logger logger = new Logger();

        def def  hooksMap = ['VMS1':'https://automation.atlassian.com/pro/hooks/b1cf09d7ba701ebb26fe66ef1f27bcffc90942f3',
                            'VMS2': 'https://automation.atlassian.com/pro/hooks/ca2a7b50c935229f3e3de810efd3d4be0681000e',
                            'VMS3': 'https://automation.atlassian.com/pro/hooks/5038861c8b9b13922fd9f5460e50d74cca4fad07'];

        public JiraNotifications(Project project) {

            this.project = project;
        }

        void tagJiraVersion() {

            if (project.getClassifier() != 'RELEASE') {
                logger.warn('SNAPSHOT will not be tagged on JIRA')
                return
            }

            def versionForJira = "${project.getArtifactVersion()}-${project.getBuildNumber()}"
            def additionalFields = '"cachingProxyFixedInBuild": "' + "${project.getProjectType().getName().replaceAll('-', '_')}_" + versionForJira + '"'
            def gitCommitMsg
            def issueId
            project.getJenkinsContext().sshagent(['jenkins-uc-global-gitlab-write-access']){
                gitCommitMsg = project.getJenkinsContext().sh(returnStdout: true, script: 'git log -1 --pretty=%B').trim()
            }

            try {
                issueId = project.getJenkinsContext().sh(returnStdout: true, script: "echo ${gitCommitMsg} | grep -Po '\\w+\\s?-\\s?\\d+'").trim().replaceAll("\\s", '')
                def body = "{\"issues\":[\"${issueId}\"], " + additionalFields + '}'
                def team = issueId.substring(issueId.indexOf("VMS")).split("-")[0]
                hooksMap.any { key, value ->
                    if (team == key) {
                        project.getJenkinsContext().httpRequest url: value, httpMode: 'POST', contentType: 'APPLICATION_JSON', requestBody: body
                        return
                    }
                }
            } catch (Exception err) {
                logger.warn("Issue id could not been extracted from ${gitCommitMsg}")
            }
        }
    }
}